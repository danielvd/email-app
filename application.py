import smtplib
from flask import Flask, Response, request
import json
import os
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

# configs
server = smtplib.SMTP('smtp.gmail.com', 587)

server.ehlo()
server.starttls()
server.login('daniel.vu.dao@gmail.com', os.environ['EMAIL_PASSWORD'])

app = Flask(__name__)

@app.route('/send', methods=['POST'])
def send():
  request_body = request.get_json()
  result = { 'success': True }
  recipient = request_body['to']
  subject = request_body['subject']

  msg = MIMEMultipart()
  msg['subject'] = subject
  msg['from'] = 'daniel.vu.dao@gmail.com'
  msg['to'] = recipient

  msg.attach(MIMEText(request_body['body'], 'plain'))

  server.send_message(msg)

  return Response(json.dumps(result), mimetype='application/json')

if __name__ == '__main__':
  app.debug = True
  port = int(os.environ.get('PORT', 5000))
  app.run(host='0.0.0.0', port=port)
  app.run()
